# Obie Mortgage Calculator

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

## Implementation details
- Built using Vue.js

## Directory Structure
```
.
├── App.vue
├── assets
│   ├── css
│   │   └── tailwind.css
│   └── logo.png
├── components
│   ├── LoanCalculator.vue
│   └── LoanDetail.vue
├── main.js
└── store.js
```
Currently, all functionality for the calculator is in `src/components/LoanCalculator.vue`

## Future Work
- Add slider UI
- Add email transporter to email form results to user
- Add validation for inputs and provide invalid-entry feedback to user
- Mask inputs
- Move `data()` from `LoanCalculator.vue` to the store
- Split `LoanCalculator.vue` into `LoanInput.vue`, `LoanBreakdown.vue`, and `AmortizationTable.vue`
- Handle environment vars for QUANDL
- Add unit tests
- Add e2e tests
