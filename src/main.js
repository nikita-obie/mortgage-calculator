import Vue from 'vue'
import App from './App.vue'
import store from './store'

import VCalendar from 'v-calendar'

import '@/assets/css/tailwind.css'

Vue.config.productionTip = false

Vue.use(VCalendar)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
