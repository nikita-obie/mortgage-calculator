let express = require('express'),
  path = require('path'),
  nodeMailer = require('nodemailer'),
  bodyParser = require('body-parser'),
  createError = require('http-errors'),
  helmet = require('helmet'),
  http = require('http');

const app = express()

// Set up middleware
app.use(express.static('src'));
app.use(helmet())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json())

// set up our API endpoint
app.post('/email', function (req, res) {
  let transporter = nodeMailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: 'obiemortgagecalculator@gmail.com',
      pass: 'obiemortgageada1'
    }
  })

  let mailOptions = {
    to: 'nparikh6@me.com',
    subject: req.body.subject,
    body: req.body.message
  }
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error)
    }
    console.log('Message %s sent: %s', info.messageId, info.response)
    console.log(info.accepted, info.rejected)
  })
  res.writeHead(301, { location: '' })
  res.end();
})

// catch 404s and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  res.status(err.status || 500)
    .json({
      error: err,
      details: err.message
    })
})

let server = app.listen(3000, function () {
  let port = server.address().port;
  console.log('Server running at https://localhost:%s', port)
})
